package demo.spring_boot.dao;

import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertTrue;

import demo.spring_boot.domain.User;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class UserDaoTest {

    @Autowired
    private UserDao userDao;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @BeforeEach
    public void setUp() {
        jdbcTemplate.update("insert into user values('1')");
    }

    @AfterEach
    public void cleanUp() {
        jdbcTemplate.update("delete from user;");
    }

    @Test
    public void testRead(){
        final Optional<User> user = userDao.findById("1");
        assertTrue(user.isPresent());
    }
}