package demo.spring_boot.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name = "USERS")
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    String id;
}
