package demo.spring_boot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import demo.spring_boot.domain.User;

@Repository
public interface UserDao extends CrudRepository<User, String> {
}
