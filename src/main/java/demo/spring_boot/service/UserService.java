package demo.spring_boot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import demo.spring_boot.dao.UserDao;
import demo.spring_boot.domain.User;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserService implements UserServiceIntf {

    private UserDao userDao;

    @Override
    public void saveUserWithId(final String userId) {
        log.info("Create User [{}]", userId);
        userDao.save(new User(userId));
    }

    @Override
    public User findById(final String userId) {
        log.info("Get User [{}]", userId);
        return userDao.findById(userId).get();
    }
}
