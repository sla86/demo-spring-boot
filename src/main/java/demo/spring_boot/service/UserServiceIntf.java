package demo.spring_boot.service;

import demo.spring_boot.domain.User;

public interface UserServiceIntf {

    public void saveUserWithId(String userId);

    public User findById(String userId);
}
